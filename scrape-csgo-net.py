from os import makedirs
from os.path import basename, join
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import selenium.webdriver.support.expected_conditions as EC
# from selenium import webdriver
import undetected_chromedriver as uc
from bs4 import BeautifulSoup
import re
from urllib.parse import urljoin
import json

from scraping import DRIVER_OPTIONS, save_screenshot

NAME = 'csgo-net'
URL = 'https://csgo.net/'

def is_crate_url(url: str):
    if re.search('/case/.+', url):
        return True
    else:
        return False

# Prepare webdriver
driver = uc.Chrome(options=DRIVER_OPTIONS)

print(f'{NAME} ({URL})')

output_dir = join('data', NAME)
makedirs(output_dir, exist_ok=True)

# Go to website
driver.get(URL)

# Wait for it to load properly
print('Waiting for website to load...')
WebDriverWait(driver, timeout=30).until(EC.presence_of_element_located((By.CLASS_NAME, 'layout-home')))
print(driver.title)

# Get main page HTML
print('Fetching main page HTML...')
main_html = driver.page_source

# Save page to disk
print('Saving it to disk...')
with open(join(output_dir, 'main.html'), "w") as file:
    file.writelines(main_html)

# Save screenshot of page
print('Saving main page screenshot...')
save_screenshot(driver, join(output_dir, 'main.png'))

# Parse main page HTML
print('Parsing main page HTML...')
soup = BeautifulSoup(main_html, 'html.parser')
del main_html

# Find all crate URLs
print('Finding links to available crates...')
all_urls = map(lambda e: e.get('href'), soup.find_all('a'))
crates_urls = list(map(lambda l: urljoin(URL, l), filter(is_crate_url, all_urls)))
del soup
print(crates_urls)

# Save URLs to disk
print('Saving crates URLs to disk...')
with open(join(output_dir, 'crates-urls.txt'), 'w') as file:
    for url in crates_urls:
        file.write(f'{url}\n')

# Loop on crates URL and fetch its details
print('Fetching crates JSON data...')
json_output_dir = join(output_dir, 'crates', 'json')
makedirs(json_output_dir, exist_ok=True)
for url in crates_urls:
    name = basename(url)
    print(f'{name} ({url})')

    # Visit page
    driver.get(url)

    # Wait for contents to load
    WebDriverWait(driver, timeout=30).until(EC.presence_of_element_located((By.CLASS_NAME, 'layout-case__skins')))
    print(driver.title)

    soup = BeautifulSoup(driver.page_source)
    skins_grid = soup.find('div', class_='grid')
    for element in skins_grid.findAll('div', recursive=False):
        title = element.find('div', class_='skin__type').get_text().strip()
        print(f'{title = }')
        subtitle = element.find('div', class_='skin__name').get_text().strip()
        print(f'{subtitle = }')
        price_str = element.find('span', class_='price-USD').get_text().strip()
        # print(f'{price_str = }')
        price = float(price_str)
        print(f'{price = }')
        odds_str = element.find(text=re.compile('Odds:')).get_text().strip()
        # print(f'{odds_str = }')
        odds = float(re.search('Odds: (.+)%', odds_str).group(1))
        print(f'{odds = }')
    exit()
