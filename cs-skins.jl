include("money.jl")

@enum WearRating begin
    BattleScarred
    WellWorn
    FieldTested
    MinimalWear
    FactoryNew
end

const WEAR_RATING_STRING = Dict{WearRating,String}(
    BattleScarred => "BS",
    WellWorn => "WW",
    FieldTested => "FT",
    MinimalWear => "MW",
    FactoryNew => "FN"
)

@inline wear_rating(str::AbstractString) =
    findfirst(==(str), WEAR_RATING_STRING)

struct Skin
    type::String
    subtype::String
    wear::WearRating
    price::MoneyAmount

    @inline Skin(; type::String, subtype::String, wear::WearRating, price::MoneyAmount) =
        new(type, subtype, wear, price)
end

@inline function Base.show(io::IO, skin::Skin)
    print(io, "$(skin.type) | $(skin.subtype)")
    if !isnothing(skin.wear)
        print(io, " [$(WEAR_RATING_STRING[skin.wear])]")
    end
    print(io, " ($(skin.price))")
end

@inline cents(skin::Skin) = skin.price.cents
