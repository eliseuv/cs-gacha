using JSON

include("cs-skins.jl")
include("rangesampler.jl")

Item = Union{Skin,MoneyAmount}

Drop = Pair{<:Integer}

struct Crate
    name::String
    price::MoneyAmount
    drops::AbstractVector{<:Drop}
    lenght::Integer

    Crate(; name::String, price::MoneyAmount, drops::AbstractVector{<:Drop}, lenght::Integer) =
        new(name, price, drops, lenght)
end

@inline function Base.show(io::IO, crate::Crate)
    println(io, "$(crate.name) ($(crate.price))")
    for drop ∈ crate.drops
        print(io, '\t')
        println(io, drop)
    end
end

function parse_keydrop_json(json_path::AbstractString)
    crate_dict = JSON.parsefile(json_path)
    low = minimum(pf["intervalFrom"] for items_dict ∈ crate_dict["items"] for pf ∈ items_dict["pf"])
    high = maximum(pf["intervalTo"] for items_dict ∈ crate_dict["items"] for pf ∈ items_dict["pf"])
    lenght = high - low + 1

    wear = [strip(pf["rarity"]) for items_dict ∈ crate_dict["items"] for pf ∈ items_dict["pf"]]

    return Crate(name=String(strip(crate_dict["title"])),
                 price=MoneyAmount(value=crate_dict["price"], currency=USD),
                 drops=[let low = pf["intervalFrom"], high = pf["intervalTo"],
                            drop_length = high - low + 1,
                            price = MoneyAmount(value=pf["price"], currency=USD),
                            rarity_str = strip(pf["rarity"]),
                            item = if rarity_str == ""
                                price
                            else
                                Skin(type=String(strip(items_dict["title"])),
                                     subtype=String(strip(items_dict["subtitle"])),
                                     wear=wear_rating(rarity_str),
                                     price=price)
                            end
                            drop_length => item
                        end
                        for items_dict ∈ crate_dict["items"] for pf ∈ items_dict["pf"]],
                 lenght=lenght)
end

function wins_sampler(crate::Crate)
    drops = sort(crate.drops, by=d -> cents(d.second))
    support = Int64[cents(drops[1].second)]
    splits = Int64[drops[1].first]
    for (length, item) ∈ drops[2:end]
        price = cents(item)
        if price == support[end]
            splits[end] += length
        else
            push!(support, price)
            push!(splits, splits[end] + length)
        end
    end
    return RangeSampler(support .- crate.price.cents, splits, check_args=false)
end
