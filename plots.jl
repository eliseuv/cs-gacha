using JLD2, StatsBase, CairoMakie, LaTeXStrings

include("keydrop.jl")

function returns_analysis(crate::Crate, n_runs::Integer)
    sampler = wins_sampler(crate)
    return rand(sampler, n_runs) ./ crate.price.cents
end

@inline function mc_hit_time(total::T, spl::RangeSampler{I,T}) where {I<:Integer,T<:Real}
    s = zero(T)
    n = 0
    while s < total
        s += rand(spl)
        n += 1
    end

    return (n, s / total)
end

mc_hit_time_analysis(crate::Crate, n_runs::Integer) =
    map(1:n_runs) do _
        mc_hit_time(crate.price.cents, wins_sampler(crate))
    end

for (root, _, filenames) in walkdir("data/key-drop/crates/parsed/")
    for filename in filenames
        path = joinpath(root, filename)

        @info "Loading crate data..." filename
        crate::Crate = load_object(path)
        println(crate)

        price = float(crate.price)

        output_dir = joinpath("./plots/key-drop", "$(crate.name)_($(price)usd)")
        mkpath(output_dir)

        @info "Return histogram..."
        let n_runs = 10000000, rets = returns_analysis(crate, n_runs)
            @info "Plotting.."
            fig = Figure(resolution=(800, 600))
            Label(
                fig[begin-1, :],
                L"%$(crate.name) ($%$(price)$ USD) $N = %$(n_runs)$",
                fontsize = 36,
                padding = (0, 0, 0, 0),
            )
            ax = Axis(fig[1,1],
                      title="Single unlock return",
                      xlabel=L"r",
                      ylabel=L"\rho(r)",
                      limits=((0, nothing), (1, nothing)),
                      yscale=Makie.pseudolog10)
            hist!(ax, rets, bins=128)
            save(joinpath(output_dir, "returns.pdf"), fig)
        end

        @info "MC hit time analysis..."
        let n_runs = 10000000, hit_times = mc_hit_time_analysis(crate, n_runs)
            @info "Plotting.."
            fig = Figure(resolution=(800, 800))
            Label(
                fig[begin-1, :],
                L"%$(crate.name) ($%$(price)$ USD) $N = %$(n_runs)$",
                fontsize = 36,
                padding = (0, 0, 0, 0),
            )
            ax_tries = Axis(fig[1,1],
                            title="Unlocks",
                            xlabel=L"n",
                            ylabel=L"\rho(n)",
                            limits=((nothing, nothing), (0, nothing)))
            try_dist = counts(first.(hit_times)) ./ n_runs
            barplot!(ax_tries, try_dist)
            ax_returns = Axis(fig[2,1],
                              title="Returns",
                              xlabel=L"r",
                              ylabel=L"\rho(r)",
                              limits=((0, nothing), (1, nothing)),
                              yscale=Makie.pseudolog10)
            hist!(ax_returns, map(r -> r[2], hit_times), bins=128, normalization=:pdf)
            save(joinpath(output_dir, "hit_time.pdf"), fig)
        end

    end
end

@info "Done"
