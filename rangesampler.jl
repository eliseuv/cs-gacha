using Distributions: @check_args
using Random, StatsBase, Distributions

struct RangeSampler{I<:Integer,T} <: Sampleable{Univariate,Discrete}
    support::AbstractVector{T}
    splits::AbstractVector{I}
    n::I

    function RangeSampler{I,T}(support::AbstractVector{T}, splits::AbstractVector{I}; check_args::Bool=true) where {I<:Integer,T}
        check_args || return new{I,T}(support, splits, splits[end])
        @check_args(
            RangeSampler,
            (length(splits) == length(support), "length of support and splits vector must be equal"),
        )
        sort_order = sortperm(splits)
        new{I,T}(support[sort_order], splits[sort_order], splits[sort_order][end])
    end
end

RangeSampler(support::AbstractVector{T}, splits::AbstractVector{I}; check_args::Bool=true) where {I<:Integer,T} =
    RangeSampler{I,T}(support, splits; check_args=check_args)

@inline function Distributions.rand(rng::AbstractRNG, spl::RangeSampler)
    draw = rand(rng, 1:spl.n)
    for (s, x) ∈ zip(spl.splits, spl.support)
        s >= draw && return x
    end
end
