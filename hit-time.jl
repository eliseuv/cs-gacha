using JLD2, StatsBase, CairoMakie, LaTeXStrings

include("keydrop.jl")

@inline function mc_hit_time(spl::RangeSampler{I,T}; n_max::Integer=Int64(1e6)) where {I<:Integer,T<:Real}
    w = zero(T)
    for n ∈ 1:n_max
        w += rand(spl)
        w >= 0 && return n => w
    end
    return n_max => w
end

mc_hit_time_analysis(crate::Crate, n_runs::Integer) =
    let spl = wins_sampler(crate)
        map(1:n_runs) do i
            n, w = mc_hit_time(spl)
            spent = n * crate.price.cents
            r = (spent + w) / spent
            return n => r
            # ht = n => r
            # println("$i: $ht")
            # return ht
        end
    end

const crate_path = ARGS[1]
const n_runs = parse(Int64, ARGS[2])
@show crate_path n_runs


@info "Loading crate data..."
const crate = load_object(crate_path)
println(crate)

const crate_name = splitext(basename(crate_path))[1]
const crate_price = float(crate.price)

const output_path = joinpath("./data/key-drop/hit_times", "$(crate_name)_($(crate_price)usd).jld2")
mkpath(dirname(output_path))

@info "MC hit time analysis..."
hit_times = mc_hit_time_analysis(crate, n_runs)

@info "Saving data..." output_path
save_object(output_path, hit_times)
