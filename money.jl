@enum Currency begin
    USD
    BRL
    EUR
end

const CURRENCY_STRING = Dict{Currency,String}(
    USD => "\$",
    BRL => "R\$",
    EUR => "€"
)

@inline function Base.show(io::IO, currency::Currency)
    print(io, CURRENCY_STRING[currency])
end

struct MoneyAmount
    currency::Currency
    cents::Integer

    MoneyAmount(currency::Currency, cents::Integer) = new(currency, cents)
    MoneyAmount(; cents::Integer, currency::Currency=USD) = new(currency, cents)
    MoneyAmount(; value::Real, currency::Currency) = new(currency, trunc(Int64, value*100))
end

@inline function Base.isless(x::MoneyAmount, y::MoneyAmount)
    @assert x.currency == y.currency
    return x.cents < y.cents
end

@inline function Base.:+(x::MoneyAmount, y::MoneyAmount)
    @assert x.currency == y.currency
    return MoneyAmount(x.currency,
                       x.cents + y.cents)
end

@inline Base.:-(amount::MoneyAmount) = MoneyAmount(amount.currency, -amount.cents)

@inline Base.:*(x::Real, m::MoneyAmount) =
    MoneyAmount(m.currency, trunc(Int64, x * m.cents))

@inline Base.:/(m::MoneyAmount, x::Real) =
    MoneyAmount(m.currency, trunc(Int64, m.cents / x))

@inline Base.zero(m::MoneyAmount) =
    MoneyAmount(m.currency, 0)

@inline function Base.show(io::IO, amount::MoneyAmount)
    symb_str = CURRENCY_STRING[amount.currency]
    if amount.cents < 0
        cents_str = string(amount.cents)[2:end]
        amount_str = cents_str[begin:end-2] * '.' * cents_str[end-1:end]

        print(io, "- " * symb_str * amount_str)

    else
        cents_str = lpad(amount.cents, 3, '0')
        amount_str = cents_str[begin:end-2] * '.' * cents_str[end-1:end]

        print(io, symb_str * amount_str)

    end

end

@inline cents(money::MoneyAmount) = money.cents

@inline float(money::MoneyAmount) = Float64(money.cents / 100)

function parse_money_amount(str::AbstractString)::MoneyAmount
    re_brl = r"[+-]?\W*(?<currency>.+?)\W*(?<amount>[\d\.]+,\d{2})"
    if (m = match(re_brl, str))
        amount_cent = parse(Int64, replace(m[:amount], "R\$" => "", ',' => "", '.' => ""))
        return MoneyAmount(BRL, amount_cent)
    end
end
