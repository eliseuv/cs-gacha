### A Pluto.jl notebook ###
# v0.19.27

using Markdown
using InteractiveUtils

# ╔═╡ 652665e8-5897-11ee-145e-a14c16b3a36c
begin
	using Pkg
	Pkg.activate(".")
end

# ╔═╡ 1d787b2e-0985-485f-848a-4a32cbf6b742
using JLD2, CairoMakie

# ╔═╡ 19fde463-79e0-442e-8677-cb72ea98f026
include("Crates.jl")

# ╔═╡ edd2f9ab-1095-43f3-9c5e-ff5d632670d9
function hit_time_analysis(crate::Crate, n_runs::Integer)
    total = crate.price.cents
    dist = distribution(get_wins(crate))
    return map(1:n_runs) do _
        mc_first_hit_time(total, dist)
    end
end

# ╔═╡ 140c5670-59c6-4606-8764-d5f30b516048
crate = load_object("data/keydrop/crates/parsed/LORE.jld2")

# ╔═╡ 10934925-31c6-4bbd-be1f-0582f4910c8c
hit_time_analysis(crate, 16)

# ╔═╡ Cell order:
# ╠═652665e8-5897-11ee-145e-a14c16b3a36c
# ╠═1d787b2e-0985-485f-848a-4a32cbf6b742
# ╠═19fde463-79e0-442e-8677-cb72ea98f026
# ╠═edd2f9ab-1095-43f3-9c5e-ff5d632670d9
# ╠═140c5670-59c6-4606-8764-d5f30b516048
# ╠═10934925-31c6-4bbd-be1f-0582f4910c8c
