from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import undetected_chromedriver as uc

# Default driver options
DRIVER_OPTIONS = Options()
DRIVER_OPTIONS.add_argument("--headless")
DRIVER_OPTIONS.add_argument("--window-size=1920,1080")

# Take screenshot of the entire page
# Ref: https://code.whatever.social/questions/41721734/take-screenshot-of-full-page-with-selenium-python-with-chromedriver#52572919
def save_screenshot(driver: uc.Chrome, path: str = '/tmp/screenshot.png') -> None:
    # Ref: https://stackoverflow.com/a/52572919/
    original_size = driver.get_window_size()
    required_width = driver.execute_script('return document.body.parentNode.scrollWidth')
    required_height = driver.execute_script('return document.body.parentNode.scrollHeight')
    driver.set_window_size(required_width, required_height)
    driver.find_element(By.TAG_NAME, 'body').screenshot(path)
    driver.set_window_size(original_size['width'], original_size['height'])
