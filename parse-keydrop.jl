using JLD2

include("keydrop.jl")

input_dir = "data/key-drop/crates/json/"
output_dir = "data/key-drop/crates/parsed/"
mkpath(output_dir)
for (root, _, filenames) in walkdir(input_dir)
    for filename in filenames
        path = joinpath(root, filename)
        @info path
        name = splitext(filename)[1]

        crate = parse_keydrop_json(path)

        println(crate)

        output_path = joinpath(output_dir, name*".jld2")
        save_object(output_path, crate)
    end
end
