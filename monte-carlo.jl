using JLD2, StatsBase, CairoMakie, LaTeXStrings

include("keydrop.jl")

function returns_analysis(crate::Crate, n_runs::Integer)
    sampler = wins_sampler(crate)
    return rand(sampler, n_runs) ./ crate.price.cents
end

@inline function mc_hit_time(spl::RangeSampler{I,T}) where {I<:Integer,T<:Real}
    x = rand(spl)
    n = 1
    while x < 0
        x += rand(spl)
        n += 1
    end

    return (n, x)
end

mc_hit_time_analysis(crate::Crate, n_runs::Integer) =
    map(1:n_runs) do _
        mc_hit_time(wins_sampler(crate))
    end

const path = ARGS[1]
const n_runs = parse(Int64, ARGS[2])

const filename = basename(path)
const name = splitext(filename)[1]

@info "Loading crate data..." filename
const crate = load_object(path)
println(crate)

const crate_price = float(crate.price)

output_dir = joinpath("./plots/key-drop", "$(crate.name)_($(price)usd)")
mkpath(output_dir)

@info "Return histogram..."
let n_runs = 1000, rets = returns_analysis(crate, n_runs)
    @info "Plotting.."
    fig = Figure(resolution=(800, 600))
    Label(
        fig[begin-1, :],
        L"%$(crate.name) ($%$(price)$ USD) $N = %$(n_runs)$",
        fontsize=36,
        padding=(0, 0, 0, 0),
    )
    ax = Axis(fig[1, 1],
        title="Single unlock return",
        xlabel=L"r",
        ylabel=L"\rho(r)",
        limits=((0, nothing), (0, nothing)))
    hist!(ax, rets, bins=128)
    save(joinpath(output_dir, "returns.pdf"), fig)
end

@info "MC hit time analysis..."
let n_runs = 1000, hit_times = mc_hit_time_analysis(crate, n_runs)
    @info "Plotting.."
    fig = Figure(resolution=(800, 800))
    Label(
        fig[begin-1, :],
        L"%$(crate.name) ($%$(price)$ USD) $N = %$(n_runs)$",
        fontsize=36,
        padding=(0, 0, 0, 0),
    )
    ax_tries = Axis(fig[1, 1],
        title="Unlocks",
        xlabel=L"n",
        ylabel=L"\rho(n)",
        limits=((nothing, nothing), (0, nothing)))
    try_dist = counts(first.(hit_times)) ./ n_runs
    barplot!(ax_tries, try_dist)
    ax_returns = Axis(fig[2, 1],
        title="Returns",
        xlabel=L"r",
        ylabel=L"\rho(r)",
        limits=((0, nothing), (0, nothing)))
    hist!(ax_returns, map(r -> r[2], hit_times), bins=128, normalization=:pdf)
    save(joinpath(output_dir, "hit_time.pdf"), fig)
end

@info "Done"
